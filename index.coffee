_ = require 'lodash'
EventEmitter = require 'eventemitter3'
if !RTCPeerConnection? or !RTCSessionDescription? or !RTCIceCandidate?
	if !window?.RTCPeerConnection? or !window?.RTCSessionDescription? or !window?.RTCIceCandidate?
		try
			webrtc = require 'wrtc'
			RTCPeerConnection = webrtc.RTCPeerConnection if !RTCPeerConnection?
			RTCSessionDescription = webrtc.RTCSessionDescription if !RTCSessionDescription?
			RTCIceCandidate = webrtc.RTCIceCandidate if !RTCIceCandidate?
		catch error
			try
				webrtc = require 'webrtc-native'
				RTCPeerConnection = webrtc.RTCPeerConnection if !RTCPeerConnection?
				RTCSessionDescription = webrtc.RTCSessionDescription if !RTCSessionDescription?
				RTCIceCandidate = webrtc.RTCIceCandidate if !RTCIceCandidate?
			catch error
				throw new Error 'webrtc-native / wrtc not found'
	else
		RTCPeerConnection = window.RTCPeerConnection
		RTCSessionDescription = window.RTCSessionDescription
		RTCIceCandidate = window.RTCIceCandidate
###

	Use:

		# Sender

			connection = new WebRTCConnection()

			## Output
			connection.on 'webrtcCandidate', (webrtcCandidate) ->
				# Relay message to the other side
			connection.on 'webrtcMessage', (binary) ->
				# yada

			## Input
			socker.io.on 'webrtcCandidate', (webrtcCandidate) ->
				connection.


			connection.initConnection()

		# Receiver

			connection = new WebRTCConnection()

			## Output
			connection.on 'webrtcCandidate', (webrtcCandidate) ->
				# Relay message to the other side
			connection.on 'webrtcMessage', (binary) ->
				# yada

			## Input
			socker.io.on 'webrtcCandidate', (webrtcCandidate) ->
				connection.

###


WebRTCConnection = (configuration={})->
	# Properties & Storage (Keep state)
	@isDataChannelReady = false
	@webRtcConfiguration = configuration
	@rooms = []
	@pc = new RTCPeerConnection @webRtcConfiguration.ice_conf, @webRtcConfiguration.optional

	# DataChannel callbacks
	@dataChannel = @pc.createDataChannel 'channel', @webRtcConfiguration.dataChannelOptions

	@dataChannel.onerror = (e) =>
		console.log 'DC errror', e
		@dconUserLeft()

	@dataChannel.onmessage = @dconMessage.bind(this)
	@dataChannel.onclose = @dconClose.bind(this)

	# Consts
	@isInitiator = false

	# Subscribe to events
	@pc.onicecandidate = @newIceCand.bind(this)
	@pc.onsignalingstatechange = @stateChangeHandler.bind(this)
	@pc.oniceconnectionstatechange = @stateChangeHandler.bind(this)
	@pc.ondatachannel = @pcondcOpen.bind(this)


	# Essential, if we get ICE before SDP
	@iceStash = []
	@sdpAdded = false
	EventEmitter.call @
	return @
WebRTCConnection.prototype = new EventEmitter()
# We don't include this into constructor, because we may need to create object without local offer. E.g. When a new peer "receives a call".
WebRTCConnection::close = (cb)->
	@pc.close cb
WebRTCConnection::initConnection = ->
	@pc.createOffer @localDescCreated.bind(this), console.warn, @webRtcConfiguration.offerOptions
	@isInitiator = true

# Step 1. On created local SDP and send it to the cloud
# This is very important concept. Anything that we create MUST be sent to the other peer.
# SDP exchange will stop ONLY when sombody _receives_ an answer
WebRTCConnection::localDescCreated = (desc) ->
	@pc.setLocalDescription desc, @onLocalSdpReady.bind(@), console.error


WebRTCConnection::addWebRtcCandidate = (webrtcCandidate) ->
	if webrtcCandidate.type is 'ICE'
		@addRemoteICE webrtcCandidate.ICE

	if webrtcCandidate.type is 'SDP'
		@addRemoteSDP webrtcCandidate.SDP


WebRTCConnection::onLocalSdpReady = (SDP) ->
	SDPm =
		type: 'SDP'
		# senderId: @myPeerId
		# recipientId: @remotePeerId
		# connectionId: @connectionId
		SDP: @pc.localDescription

	if !@pc.localDescription.type?
		console.error 'EMPTY_LOCAL_SDP', @pc.localDescription
		return
	@emit 'webrtcCandidate',SDPm

WebRTCConnection::addRemoteSDP = (SDP) ->
	remSDP = new RTCSessionDescription SDP
	@pc.setRemoteDescription remSDP, @onRemoteSDPadded.bind(@), console.error

WebRTCConnection::onRemoteSDPadded = (SDP) ->
	switch @pc.remoteDescription.type
		when 'offer'
			@pc.createAnswer @localDescCreated.bind(@), console.error, @webRtcConfiguration.offerOptions
		when 'pranswer'
			@pc.createAnswer @localDescCreated.bind(@), console.error, @webRtcConfiguration.offerOptions
		when 'answer'
			# Part of symmetric NAT test. Read method desc. 8 Seconds is a magic number
			# delay 8000, =>
			# 	if not @isDataChannelReady
			# 		console.warn "Suspect a symmetric NAT for #{@connectionId}"
		else
			console.error 'UNKOWN_SDP_TYPE'

	@sdpAdded = true
	@pushStashedIce()
	return

# Push statashed messages that came before first SDP
WebRTCConnection::pushStashedIce = ->
	_.forEach @iceStash, (ice) =>
		@addRemoteICE ice
	@iceStash = []

#  OnIceCandidate Event :: Send any ice candidates to the Tracker
WebRTCConnection::newIceCand = (evt) ->
	if evt.candidate
		ICEm =
			type: 'ICE'
			# senderId: @myPeerId
			# recipientId: @remotePeerId
			# connectionId: @connectionId
			ICE: evt.candidate
		@emit 'webrtcCandidate',ICEm

WebRTCConnection::sendData = (data) ->
	# Check data compability with WebRTC before sending
	toArrayBuffer = (buffer) ->
		ab = new ArrayBuffer(buffer.length)
		view = new Uint8Array(ab)
		i = 0
		while i < buffer.length
			view[i] = buffer[i]
			++i
		ab
	if data instanceof Buffer
		data = toArrayBuffer data
	try
		if @isInitiator
			# console.log 'dc send'
			@dataChannel.send data
		else
			@remoteChannel.send data
	catch err
		console.log 'Sending to remote peer (probably he has left) has failed: ', err
#  Here we get remote ICE and add it to the object
WebRTCConnection::addRemoteICE = (ICE) ->

	if @sdpAdded == false
		@iceStash.push ICE
		return false

	remICE = new RTCIceCandidate
		sdpMLineIndex: ICE.sdpMLineIndex
		sdpMid: ICE.sdpMid
		candidate: ICE.candidate

	@pc.addIceCandidate remICE, ->
		true
	, (err)->
		console.error remICE

#  On data channel open event by the Peer Connection object
WebRTCConnection::pcondcOpen = (event) ->
	@remoteChannel = event.channel
	# TODO
	@remoteChannel.onerror = @dconEroor.bind(this)
	@remoteChannel.onmessage = @dconMessage.bind(this)
	@remoteChannel.onopen = @dconOpen.bind(this)
	@remoteChannel.onclose = @dconClose.bind(this)

#  When RTCPeerConnection state changes, do something about it
WebRTCConnection::stateChangeHandler = ->
	if @pc.iceConnectionState == 'disconnected'
		@emit 'connectionClose'

# @ option error
WebRTCConnection::dconEroor = (error)->
	@emit 'connectionError',error
	@emit 'connectionClose'

WebRTCConnection::onBinary = (binary, len) ->
	# console.log 'Got binary from the wire: ', len


# Send inc data (async) to smb Module
WebRTCConnection::dconMessage = (message) ->
	mdataLen = message.data.size or message.data.byteLength
	# If we deal with BLOB (FF) we have to convert it. Othervise just send it
	if Blob? and FileReader? and message.data instanceof Blob
		fileReader = new FileReader()

		fileReader.onload = (e)=>
			@emit 'webrtcMessage',e.target.result, mdataLen
			if @listeners('webrtcMessageUint8Array').length > 0
				@emit 'webrtcMessageUint8Array',new Uint8Array(e.target.result),mdataLen
			if @listeners('webrtcMessageBuffer').length > 0 and Buffer?
				@emit 'webrtcMessageBuffer',new Buffer(e.target.result),mdataLen

		fileReader.readAsArrayBuffer message.data
	else
		@emit 'webrtcMessage',message.data, mdataLen
		if @listeners('webrtcMessageUint8Array').length > 0
			@emit 'webrtcMessageUint8Array',new Uint8Array(message.data),mdataLen
		if @listeners('webrtcMessageBuffer').length > 0 and Buffer?
			@emit 'webrtcMessageBuffer',new Buffer(message.data),mdataLen

WebRTCConnection::dconOpen = ->
	@isDataChannelReady = true
	@emit 'connectionOpen'

# Inform (async) ems Module
WebRTCConnection::onUserLeft = ->
	console.log 'user left'

WebRTCConnection::symmetricNatTest = ->
	if not @isDataChannelReady
		console.warn "Suspect a symmetric NAT for #{@connectionId}"

#Inform smb what is happening with DC or Peer Connection
WebRTCConnection::dconClose = (error) ->
	@emit 'connectionClose'

# #  Debug function
# WebRTCConnection::getConnectionStatusString = ->
# 	txt = 'ID: ' + @con nect ionId + '. ##Signaling State: ' + @pc.signalingState + '. Ice Connection State: ' + @pc.iceConnectionState + '. Ice Gathering State: ' + @pc.iceGatheringState
# 	return txt

module.exports = WebRTCConnection
